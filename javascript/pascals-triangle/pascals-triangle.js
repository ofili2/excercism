export class Triangle {
  constructor(rows) {
    var triangleRows = [];
    for (var i = 0; i < rows; i++) {
      triangleRows.push(new Array(i + 1).fill(1));
    }
    /*
    1 <- 0, 1

    1 <- 0, 1
    2 <- 1, 2
    
    1 , 1, 1, 1
   1  1  1   1   1

    1 <-0, 1
    2 <- 1, 2
    3 <- 2,3
    */

    console.log("TRIANGLE ", triangleRows);
    this.triangle = triangleRows;
  }

  get lastRow() {
    return this.triangle.slice(-1);
  }

  get rows() {
    return this.triangle;
  }
}
