const forwardLetters: Array<string> = "abcdefghijklmnopqrstuvwxyz".split("");
const backwardLetters: Array<string> = "zyxwvutsrqponmlkjihgfedcba".split("");

class AtbashCipher {
  encode = (input: string): string => {
    return input
      .replace(/[ |,|.]/g, "")
      .split("")
      .map((letter, index) => {
        // ignores numbers
        if (isNaN(Number(letter))) {
          letter =
            backwardLetters[forwardLetters.indexOf(letter.toLowerCase())];
          // undefineds will be an empty string
          if (letter === undefined) letter = "";
        }
        // If it is the last character then don't append
        if (index + 1 != input.length) {
          letter = (index + 1) % 5 === 0 ? letter + " " : letter;
        }
        return letter;
      })
      .join("");
  };

  decode = (input: string): string => {
    return input
      .replace(/ /g, "")
      .split("")
      .map(letter => {
        // ignores numbers
        if (isNaN(Number(letter))) {
          letter =
            backwardLetters[forwardLetters.indexOf(letter.toLowerCase())];
        }
        return letter;
      })
      .join("");
  };
}

export default AtbashCipher;
