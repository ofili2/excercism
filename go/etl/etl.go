package etl

import "strings"

// Transform a
func Transform(input map[int][]string) map[string]int {

	output := make(map[string]int)
	for keyValue, stringListValue := range input {
		for _, convertString := range stringListValue {
			output[strings.ToLower(convertString)] = keyValue
		}
	}

	return output
}
