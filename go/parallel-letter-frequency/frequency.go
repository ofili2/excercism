package letter

// FreqMap records the frequency of each rune in a given text.
type FreqMap map[rune]int

// Frequency counts the frequency of each rune in a given text and returns this
// data as a FreqMap.
func Frequency(s string) FreqMap {
	m := FreqMap{}
	for _, r := range s {
		m[r]++
	}
	return m
}

// ConcurrentFrequency hi
func ConcurrentFrequency(input []string) FreqMap {
	c := make(chan FreqMap)

	for idx := range input {
		go func(input string) {
			c <- Frequency(input)
		}(input[idx])
	}

	m := FreqMap{}
	for range input {
		msg := <-c
		for k, v := range msg {
			m[k] += v
		}
	}
	return m
}
