package secret

// I take advantage that an array is basically the bits being turned on
var values = []string{
	"wink",
	"double blink",
	"close your eyes",
	"jump",
}

// Handshake a
func Handshake(instruction uint) (cleartext []string) {

	for bitPostion := range values {
		if instruction&(1<<bitPostion) > 0 {
			cleartext = append(cleartext, values[bitPostion])
		}
	}

	if instruction&(1<<4) > 0 {
		temp := make([]string, len(cleartext))
		for idx := len(cleartext) - 1; idx >= 0; idx-- {
			idx2 := len(cleartext) - idx - 1
			temp[idx2] = cleartext[idx]
		}
		cleartext = temp
	}
	return cleartext
}
