package twofer

import (
	S "strings"
)

// ShareWith should have a comment documenting it.
func ShareWith(name string) string {

	if S.EqualFold(name, "") {
		return "One for you, one for me."
	}
	return S.Join([]string{"One for ", name, ", one for me."}, "")
}
